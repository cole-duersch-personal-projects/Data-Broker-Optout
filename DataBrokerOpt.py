from email.message import EmailMessage
import smtplib
import time



databrokers = {
    "Intellius": "privacy@intellius.com",
    "Exponential Interactive, Inc. doing business as VDX.tv": "privacy@VDX.tv",
    "The Alesco Group LLC": "michael@alescodata.com",
    "Response Solutions Group LLC": "michael@alescodata.com",
    "Stat Resource Group LLC DBA Statlistics": "michael@alescodata.com",
    "ZoomInfo": "privacy@zoominfo.com",
    "Blue Hill Marketing Solutions, inc": "dataprivacy@liftbasedata.com",
    "Spy Dialer, Inc. dba SpyDialer.com": "info@spydialer.com",
    "First Orion, Inc.": "cdenton@firstorion.com",
    "Oracle America, Inc., Oracle Data Cloud": "privacy_ww@oracle.com",
    "Semasio GmbH": "info@semasio.com",
    "Blackbaud, Inc.": "privacy@blackbaud.com",
    "Lender Feed LC": "CCPA@monitorbase.com",
    "Lead411 Corporation": "tom@lead411.io",
    "Gravy Analytics, Inc.": "privacy@gravyanalytics.com",
    "Cision US Inc.": "DPO@cision.com",
    "Clearview AI, Inc.": "privacy@clearview.ai",
    "HireTeamMate, Inc. dba hireEZ": "legal@hireez.com",
    "Infopay, Inc.": "jenna@infopay.com",
    "J.D. Power": "laszlo.kupan@jdpa.com",
    "Optimal Fusion": "dataprotectionofficer@optimalfusion.com",
    "RecruitBot, Inc.": "accounting@therecruitbot.com",
    "Throtle": "privacy@throtle.io",
    "Anchor Computer Inc.": "privacyteam@anchorcomputer.com",
    "Versium": "privacy@versium.com",

    "US Data Corporation": "erich@usdatacorporation.com",
    "Sawyer Lists, LLC": "admin@sawyerdatadirect.com",
    "Acxiom LLC": "consumeradvo@acxiom.com",
    "Seamless Contacts, Inc. d/b/a Seamless.AI": "privacy@seamlessleads.com",
    "ACE Agents Inc.": "admin@academixdirect.com",
    "S&P Global Inc.": "privacy@spglobal.com",
    "First Direct, Inc.": "support@firstdirectmarketing.com",
    "Hunter Web Services Inc": "privacy@hunter.io",
    "Integrated Medical Data, LLC.": "bzink@integratedmedicaldata.com",
    "Specialists Marketing Services, Inc.": "dataprivacyteam@sms-inc.com",
    "TargetSmart Communications LLC": "privacy@targetsmart.com",
    "Comscore, Inc.": "privacy@comscore.com",
    "Spokeo, Inc.": "legal@spokeo.com",
    "Cortera, Inc.": "privacy@cortera.com",
    "SourceIT Technologies, Inc": "dataprivacy@sourceitmarketing.com",
    "Email Marketing Services, Inc": "dataprivacy@listmatch.com",
    "SafeGraph Inc.": "privacy@safegraph.com",
    "Veraset": "privacy@veraset.com",
    "Belardi Wong": "Jason.bier@alc.com",
    "Adstra LLC": "privacy.officer@alc.com",
    "InMobi": "privacy@inmobi.com",
    "HERE North America LLC": "privacy@here.com",
    "Media Direct, Inc.": "Seth@LubinLawFirm.com",
    "NextRoll, Inc.": "legal@nextroll.com",
    "Pipl, Inc.": "susan.haddad@pipl.com",

    "Monevo Inc": "supportusa@monevo.com",
    "Epsilon Data Management, LLC": "privacy@epsilon.com",
    "AccuData Integrated Marketing, Inc.": "privacy.compliance@accudata.com",
    "Merkle Inc.": "dpous@merkleinc.com",
    "Compact Information Systems": "privacy.compliance@compactlists.com",
    "Claritas LLC.": "privacy@claritas.com",
    "Edvisors Network, Inc.": "customerservice@edvisors.com",
    "MaxMind, Inc.": "legal@maxmind.com",
    "Deloitte Consulting LLP": "USPrivacyQuestions@deloitte.com",
    "PacificEast Research Inc.": "privacy@pacificeast.com",
    "Adrea Rubin Marketing, Inc.": "jenniferv@adrearubin.com",
    "Adrea Rubin Media, Inc. dba Calibrant Digital": "jennifer@calibrant.com",
    "Arity 875, LLC": "privacy@arity.com",
    "Infofree.com": "bob@infofree.com",
    "Cross Pixel Media, Inc.": "privacy@crosspixel.net",
    "BeenVerified, Inc. and its subsidiaries and affiliates": "ccpa@beenverified.com",
    "Taboola, Inc.": "privacy@taboola.com",
    "Melissa Corporation": "CCPArequest@melissa.com",
    "Path2Response, LLC": "privacyinfo@path2response.com",
    "Anteriad": "privacy@anteriad.com",
    "DatabaseUSA": "Privacy@databaseusa.com",
    "All Web Leads, Inc": "onlineops@allwebleads.com",
    "All Global Resources, LLC": "privacy@agrgroupinc.com",
    "Leadspace, Inc.": "info@leadspace.com",
    "EproDirect": "team@eprodirect.com",

    "LimeLeads": "privacy@limeleads.com",
    "Media Source Solutions": "mvolpe@mediasourcesolutions.com",
    "Connected Investors": "support@connectedinvestors.com",
    "TrueData Solutions, Inc.": "privacy@truedata.co",
    "Experian Health, Inc.": "privacyrequests@experian.com",
    "Electronic Voice Services, Inc.": "richard@evs7.com",
    "Searchbug": "support@searchbug.zohodesk.com",
    "Experian Information Solutions, Inc.": "privacyrequests@experian.com",
    "Ubermedia": "privacy@ubermedia.com",
    "Apollo Interactive, Inc.": "requests@apollointeractive.com",
    "TruthFinder, LLC": "help@truthfinder.com",
    "Instant Checkmate, LLC": "support@instantcheckmate.com",
    "PubMatic, Inc.": "privacy@pubmatic.com",
    "Windfall Data, Inc.": "privacy@windfalldata.com",
    "WealthEngine, Inc.": "PIPrivacy@euromoneyplc.com",
    "Outbrain Inc.": "legal@outbrain.com",
    "Cyndx Networks LLC": "privacy@cyndx.com",
    "Nexxa Group, Inc.": "ConsumerChoice@nexxagroup.com",
    "Complete Medical Lists, Inc.": "tburnell@completemedicallists.com",
    "Complete Mailing Lists": "ewoolf@CompleteMailingLists.com",
    "Foursquare Labs, Inc.": "privacy@foursquare.com",
    "NFocus Consulting, Inc.": "consumer@n-focus.com",
    "Webbula, LLC": "support@webbula.com",
    "Martin Data LLC": "ccpa@usinfosearch.com",
    "Digital Segment, LLC": "info@digitalsegment.com"
}


LegalName = input("Please enter your legal name (First Last): ")
HomeAddress = input("Please enter your home address: ")
HomeCity = input("Please enter your home city: ")
HomeState = input("Please enter your home state: ")
HomeZip = input("Please enter your home zip: ")
DOB = input("Please enter your date of birth (YYYY-MM-DD): ")
PhoneNo = input("Please enter your phone number: ")

# Create a SMTP object and connect to the Gmail SMTP server
smtp_server = 'smtp.gmail.com'
smtp_port = 587
smtp_username = input("Please enter your email: ")
smtp_password = input("Please enter your password (If 2FA is enabled use App Password): ")

for name, email in databrokers.items():
    recipient = name
    r_email = email
    msg = EmailMessage()
    msg["From"] = f"{LegalName} <{smtp_username}>"
    msg["To"] = f"{name} <{r_email}>"
    msg["Subject"] = "Data Deletion and Optout Request"

    # Set the body of the email message
    msg.set_content(f"""Dear {name},

    I am writing to request that you delete all of my personal information from your records and to opt-out of any future sales of my information. I am making this request under the Utah Consumer Privacy Act (UCPA).

    The UCPA gives Utah residents the right to request that businesses delete their personal information and to opt-out of the sale of their personal information. I am exercising my rights under the UCPA by requesting that you delete all of my personal information from your records and to opt-out of any future sales of my information.

    My personal information includes the following:

    My name {LegalName}
    My address {HomeAddress}, {HomeCity}, {HomeState}, {HomeZip} and all other addresses
    My phone number {PhoneNo} and all other phone numbers
    My email address {smtp_username} and all other email addresses
    My date of birth {DOB}
    Any other personal information that you have collected about me
    I understand that you may need to retain some of my personal information for legitimate business purposes, such as to comply with legal requirements or to protect your interests. However, I request that you delete as much of my personal information as possible.

    Please notify me when it has been completed.

    Thank you for your time and attention to this matter.

    If this email was not sent to the proper department, please forward it to the correct department.

    Sincerely,
    {LegalName}
    """)

    try:
        with smtplib.SMTP(smtp_server, smtp_port) as server:
            server.starttls()
            server.login(smtp_username, smtp_password)
            server.send_message(msg)
        print('Email sent successfully.')
        time.sleep(1200)
    except Exception as e:
        print('An error occurred while sending the email:', str(e))
